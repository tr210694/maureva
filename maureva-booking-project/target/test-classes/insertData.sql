INSERT INTO airport (airport_s_code, airport_s_country, airport_s_region, airport_s_city)
VALUES ('MRU', 'Maurice', 'Grand Port', 'Plaine Magnien'),
       ('CDG', 'France', 'Paris', 'Charles de Gaulle');

INSERT INTO flight (flight_s_id, flight_s_carrier, flight_s_number, flight_n_origin_id, flight_n_destination_id,
                    flight_d_arrival_time, flight_d_departure_time)
VALUES ('sQTWMC3R2BKAzkvFCAAAAA==', 'MRV', '123', 1, 2, '2023-04-01T08:00:00-04:00', '2023-04-01T12:00:00-04:00');

INSERT INTO booking_info (booking_info_s_cabin_class, booking_info_n_booking_counts, flight_flight_s_id)
VALUES ('FIRST', 20, 'sQTWMC3R2BKAzkvFCAAAAA=='),
       ('PREMIUMECONOMY', 30, 'sQTWMC3R2BKAzkvFCAAAAA=='),
       ('ECONOMY', 50, 'sQTWMC3R2BKAzkvFCAAAAA==');

INSERT INTO users ( user_n_active, user_s_email, user_s_firstname, user_s_lastname, user_s_password,
                    user_s_username)
VALUES ( 1, 'teshandramdoo@gmail.com', 'Teshand', 'ramdoo',
         '$2a$10$HS/E5fAMmz2C5YnZ9tJEaOFnwhNpyg7ae1PhW6R7JrYEuPubbM5qa', 'ter');

INSERT INTO authority (authority_n_id, authority_s_name) VALUES (1, 'ROLE_USER');
INSERT INTO authority (authority_n_id, authority_s_name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO user_authorities (user_authority_user_n_id, user_authority_authority_n_id) VALUES (1, 1);

INSERT INTO flight_booking (flight_booking_s_booking_ref, flight_booking_s_firstname, flight_booking_s_lastname,
                            flight_booking_s_email, flight_booking_s_phone_number, flight_booking_s_passport_number,
                            flight_booking_s_cabin_class, flight_booking_n_booked_by, flight_booking_s_flight)
VALUES ('Booking ref 123', 'Teshand', 'Ramdoo', 'Teshand@gmail.com', '+23052505565', '192636352', 'ECONOMY', 1, 'sQTWMC3R2BKAzkvFCAAAAA==');