DO $$ DECLARE
    tabname TEXT;
BEGIN
    FOR tabname IN (SELECT tablename FROM pg_tables WHERE schemaname = 'public') LOOP
            EXECUTE 'TRUNCATE TABLE ' || tabname || ' CASCADE;';
        END LOOP;
END $$;