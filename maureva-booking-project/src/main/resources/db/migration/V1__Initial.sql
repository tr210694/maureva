
create table if not exists  airport
(
    airport_n_id      bigserial   not null
        constraint airport_pkey
            primary key,
    airport_s_code    varchar(3)  not null
        constraint uk_hfm0oyirqi1jl9438dfq9ytjq
            unique,
    airport_s_city    varchar(50) not null,
    airport_s_country varchar(50) not null,
    airport_s_region  varchar(50) not null
);

create table if not exists  authority
(
    authority_n_id   bigserial   not null
        constraint authority_pkey
            primary key,
    authority_s_name varchar(50) not null
);

create table if not exists  users
(
    user_n_id        bigserial          not null
        constraint users_pkey
            primary key,
    user_n_active    smallint default 1 not null,
    user_s_email     varchar(255)       not null
        constraint uk_g0778wo8rwq8na2h11q3pcnqv
            unique,
    user_s_firstname varchar(255)       not null,
    user_s_lastname  varchar(255)       not null,
    user_s_password  varchar(255)       not null,
    user_s_username  varchar(255)       not null
        constraint uk_1wdmnafmsjapflxj0asa1up7d
            unique
);

create table if not exists flight
(
    flight_s_id              varchar(255) not null
        constraint flight_pkey
            primary key,
    "flight_d_arrival_time"   timestamp    not null,
    flight_s_carrier         varchar(3)   not null,
    "flight_d_departure_time" timestamp    not null,
    flight_s_number          varchar(25)  not null,
    flight_n_destination_id  bigint
        constraint fk_flight_destination_airport
            references airport,
    flight_n_origin_id       bigint
        constraint fk_flight_origin_airport
            references airport
);

create table if not exists  booking_info
(
    booking_info_n_id             bigserial not null
        constraint booking_info_pkey
            primary key,
    booking_info_n_booking_counts integer,
    booking_info_s_cabin_class    varchar(255),
    flight_flight_s_id            varchar(255)
        constraint fkjj0s9d1u57d2bmrjmvddyrxd1
            references flight
);

create table if not exists  user_authorities
(
    user_authority_user_n_id      bigint not null
        constraint fk_userauthority_user
            references users,
    user_authority_authority_n_id bigint not null
        constraint fk_userauthority_authority
            references authority,
    constraint user_authorities_pkey
        primary key (user_authority_user_n_id, user_authority_authority_n_id)
);

create table if not exists flight_booking
(
    flight_booking_n_id              bigint       not null
        constraint flight_booking_pkey
            primary key,
    flight_booking_s_booking_ref     varchar(255) not null
        constraint uk_8rusflfmpcogl0v2ffq6pu196
            unique,
    flight_booking_s_cabin_class     varchar(255),
    flight_booking_s_email           varchar(255) not null,
    flight_booking_s_firstname       varchar(255) not null,
    flight_booking_s_lastname        varchar(255) not null,
    flight_booking_s_passport_number varchar(255) not null,
    flight_booking_s_phone_number    varchar(255) not null,
    flight_booking_n_booked_by       bigint       not null
        constraint fk_flight_booking_user
            references users,
    flight_booking_s_flight          varchar(255)
        constraint fk_flight_booking_flight
            references flight
);

INSERT INTO users ( user_n_active, user_s_email, user_s_firstname, user_s_lastname, user_s_password,
                    user_s_username)
VALUES ( 1, 'teshandramdoo@gmail.com', 'Teshand', 'ramdoo',
         '$2a$10$HS/E5fAMmz2C5YnZ9tJEaOFnwhNpyg7ae1PhW6R7JrYEuPubbM5qa', 'ter');

INSERT INTO authority (authority_n_id, authority_s_name) VALUES (1, 'ROLE_USER');
INSERT INTO authority (authority_n_id, authority_s_name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO user_authorities (user_authority_user_n_id, user_authority_authority_n_id) VALUES (1, 1);
