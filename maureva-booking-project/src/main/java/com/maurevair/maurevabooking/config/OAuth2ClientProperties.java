package com.maurevair.maurevabooking.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "oauth")
@Getter
@Setter
public class OAuth2ClientProperties {

    @NotEmpty
    private List<ClientProperties> clients;

    @Getter
    @Setter
    public static class ClientProperties {
        @NotEmpty
        private String clientId;
        @NotEmpty
        private String clientSecret;
        @NotEmpty
        private String[] authorizedGrantTypes;
        private String[] scopes;
        private int accesTokenValiditySeconds;
        private int refreshTokenValiditySeconds;
        private String[] authorities;
    }
}

