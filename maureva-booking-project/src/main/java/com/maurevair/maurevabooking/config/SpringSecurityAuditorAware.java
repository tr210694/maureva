package com.maurevair.maurevabooking.config;

import com.maurevair.maurevabooking.domain.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityAuditorAware implements AuditorAware<User> {

	@Override
	public Optional<User> getCurrentAuditor() {
	    return UserUtils.getConnectedUser() != null ? Optional.of(UserUtils.getConnectedUser().getUser()) : Optional.empty();
	}
}