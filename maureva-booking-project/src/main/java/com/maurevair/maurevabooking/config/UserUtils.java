package com.maurevair.maurevabooking.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtils {
	
	private UserUtils() {
	}
	
	public static JwtUser getConnectedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (authentication == null || !authentication.isAuthenticated()) {
	    	return null;
	    }
	    return (JwtUser) authentication.getPrincipal();
	}
}