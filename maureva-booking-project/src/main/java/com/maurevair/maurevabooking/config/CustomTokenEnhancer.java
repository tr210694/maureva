package com.maurevair.maurevabooking.config;

import com.maurevair.maurevabooking.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

	@Autowired
	private Environment env;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
			JwtUser user = (JwtUser) authentication.getPrincipal();		
			user.setUser(this.createEnhanceUser(user.getUser()));
			Map<String, Object> additionalInfo = new HashMap<>();
			additionalInfo.put("currentUser", user);
			additionalInfo.put("refresh_expires_in", Integer.valueOf(env.getProperty("security.oauth2.client.refresh-token-validity-seconds", String.valueOf(OAuth2AuthorizationServerConfig.REFRESH_TOKEN_VALIDITY_SECONDS))));
			((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}
	
	/**
	 * Include the logged user that is requested for the front end and that is included as additional information on the token
	 * @param loggedUser
	 * @return - The enhanced user
	 */
	private User createEnhanceUser(User loggedUser) {
		User currentUser = new User();
		currentUser.setId(loggedUser.getId());
		currentUser.setFirstName(loggedUser.getFirstName());
		currentUser.setLastName(loggedUser.getLastName());
		currentUser.setUsername(loggedUser.getUsername());
		currentUser.setActive(loggedUser.isActive());
		currentUser.setEmail(loggedUser.getEmail());
		currentUser.setEmail(loggedUser.getEmail());
		currentUser.setAuthorities(loggedUser.getAuthorities());
		return currentUser;
	}

}
