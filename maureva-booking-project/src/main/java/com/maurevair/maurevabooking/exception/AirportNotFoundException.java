package com.maurevair.maurevabooking.exception;

public class AirportNotFoundException extends Exception {
    public AirportNotFoundException(String message) {
        super(message);
    }
}
