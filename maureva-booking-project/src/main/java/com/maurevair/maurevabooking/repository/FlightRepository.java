package com.maurevair.maurevabooking.repository;

import com.maurevair.maurevabooking.domain.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, String> {
    @Query("select distinct f from Flight  f " +
            "inner join fetch f.origin oa" +
            "inner join fetch f.destination da" +
            "inner join fetch f.bookingInfos" +
            " where f.origin.id = :originAirportId" +
            " and f.destination.id = :destinationAirportId")
    List<Flight> findAllFlightsBetweenOriginAndDestination(@Param("originAirportId") Long originAirportId, @Param("destinationAirportId") Long destinationAirportId);

    @Query("select distinct f from Flight  f " +
            "inner join fetch f.origin oa" +
            "inner join fetch f.destination da" +
            "inner join fetch f.bookingInfos")
    List<Flight> loadAllAvailableFlights();

}

