package com.maurevair.maurevabooking.repository;

import com.maurevair.maurevabooking.domain.FlightBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightBookingRepository extends JpaRepository<FlightBooking , Long> {


}
