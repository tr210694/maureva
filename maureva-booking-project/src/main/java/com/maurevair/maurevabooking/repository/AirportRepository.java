package com.maurevair.maurevabooking.repository;

import com.maurevair.maurevabooking.domain.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long> {

    Airport findAirportByAirportCode(String airportCode);
}
