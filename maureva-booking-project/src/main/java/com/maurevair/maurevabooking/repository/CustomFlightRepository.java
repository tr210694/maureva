package com.maurevair.maurevabooking.repository;

import com.maurevair.maurevabooking.domain.BookingInfo;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class CustomFlightRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<AvailableFlightBookingDto> findAvailableSeatsForFlightByCabinClass(String flightId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AvailableFlightBookingDto> cq = cb.createQuery(AvailableFlightBookingDto.class);
//        1. join flight and booking info
        Root<Flight> flight = cq.from(Flight.class);
        Join<Flight, BookingInfo> bi = flight.join("bookingInfos", JoinType.INNER);

        //2.create sub query to get count of flight booking for a specific flight where cabin class in booking info matches with cabin class on flight booking
        Subquery<Long> fbSubquery = cq.subquery(Long.class);
        Root<FlightBooking> fbSubqueryRoot = fbSubquery.from(FlightBooking.class);
        fbSubquery.select(cb.count(fbSubqueryRoot));
        fbSubquery.where(cb.and(
                cb.equal(fbSubqueryRoot.get("flight"), flight),
                cb.equal(fbSubqueryRoot.get("cabinClass"), bi.get("cabinClass"))
        ));

        cq.select(cb.construct(
                AvailableFlightBookingDto.class,
                flight.get("id"),
                bi.get("cabinClass"),
                cb.diff(bi.get("bookingCounts"), fbSubquery)
        ));
        cq.where(cb.equal(flight.get("id"), flightId));
        cq.groupBy(flight.get("id"), bi.get("cabinClass"), bi.get("bookingCounts"));

        TypedQuery<AvailableFlightBookingDto> query = entityManager.createQuery(cq);
        return query.getResultList();

    }
}
