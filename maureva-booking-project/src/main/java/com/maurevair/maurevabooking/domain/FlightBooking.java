package com.maurevair.maurevabooking.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "flight_booking")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class FlightBooking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "flight_booking_n_id")
    private Long id;

    @Column(name = "flight_booking_s_booking_ref", nullable = false , unique = true)
    private String bookingReference;

    @Column(name = "flight_booking_s_firstname", nullable = false)
    private String firstName;

    @Column(name = "flight_booking_s_lastname", nullable = false)
    private String lastName;

    @Column(name = "flight_booking_s_email", nullable = false)
    private String email;

    @Column(name = "flight_booking_s_phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "flight_booking_s_passport_number", nullable = false)
    private String passportNumber;

    @Enumerated(EnumType.STRING)
    @Column(name="flight_booking_s_cabin_class")
    private CabinClass cabinClass;


    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_booking_n_booked_by", foreignKey = @ForeignKey(name = "FK_FLIGHT_BOOKING_USER"), nullable = false)
    private User bookedBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_booking_s_flight", foreignKey = @ForeignKey(name = "FK_FLIGHT_BOOKING_FLIGHT"))
    private Flight flight;
}
