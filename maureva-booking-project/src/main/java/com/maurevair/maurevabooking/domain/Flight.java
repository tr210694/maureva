package com.maurevair.maurevabooking.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maurevair.maurevabooking.adapter.ZonedDateTimeAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "flight")
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Flight {

    @Id
    @Column(name = "flight_s_id")
    @XmlAttribute(name = "id")
    private String id;

    @Column(name = "flight_s_carrier", length = 3, nullable = false)
    @XmlAttribute(name = "Carrier")
    private String carrier;

    @Column(name = "flight_s_number", length = 25, nullable = false)
    @XmlAttribute(name = "FlightNumber")
    private String flightNumber;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "flight_n_origin_id", foreignKey = @ForeignKey(name = "FK_FLIGHT_ORIGIN_AIRPORT"))
    @XmlTransient
    private Airport origin;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "flight_n_destination_id", foreignKey = @ForeignKey(name = "FK_FLIGHT_DESTINATION_AIRPORT"))
    @XmlTransient
    private Airport destination;

    @Column(name = "flight_d_arrival_time", nullable = false)
    @XmlAttribute(name = "ArrivalTime")
    @XmlJavaTypeAdapter(ZonedDateTimeAdapter.class)
    private ZonedDateTime arrivalTime;

    @Column(name = "flight_d_departure_time", nullable = false)
    @XmlAttribute(name = "DepartureTime")
    @XmlJavaTypeAdapter(ZonedDateTimeAdapter.class)
    private ZonedDateTime departureTime;

    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @XmlElement(name = "BookingInfo")
    private List<BookingInfo> bookingInfos;

    @Transient
    @JsonIgnore
    @XmlAttribute(name = "Origin")
    private String originAirportCode;

    @Transient
    @JsonIgnore
    @XmlAttribute(name = "Destination")
    private String destinationAirportCode;

    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @XmlTransient
    @JsonBackReference
    private List<FlightBooking> flightBookings;


}
