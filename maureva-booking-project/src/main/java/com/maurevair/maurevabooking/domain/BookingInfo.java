package com.maurevair.maurevabooking.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.maurevair.maurevabooking.adapter.CabinClassAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity
@Getter
@Setter
@Table(name = "booking_info")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="booking_info_n_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name="booking_info_s_cabin_class")
    @XmlAttribute(name = "CabinClass")
    @XmlJavaTypeAdapter(CabinClassAdapter.class)
    private CabinClass cabinClass;

    @Column(name = "booking_info_n_booking_counts")
    @XmlAttribute(name = "SeatsAvailable")
    private Integer bookingCounts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Flight flight;
}
