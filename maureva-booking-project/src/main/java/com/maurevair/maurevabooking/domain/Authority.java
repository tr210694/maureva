package com.maurevair.maurevabooking.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authority_n_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "authority_s_name", length = 50, nullable = false)
    private AuthorityName name;
}
