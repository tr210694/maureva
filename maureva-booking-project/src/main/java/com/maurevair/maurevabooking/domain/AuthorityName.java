package com.maurevair.maurevabooking.domain;

public enum AuthorityName {

    ROLE_USER("ROLE_USER", 0),
    ROLE_ADMIN("ROLE_ADMIN", 1);

    private AuthorityName(String name, int level) {
        this.name = name;
        this.level = level;
    }

    private String name;

    private int level;

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

}
