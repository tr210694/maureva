package com.maurevair.maurevabooking.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_n_id")
    private Long id;

    @Column(name="user_s_firstname", nullable = false)
    private String firstName;

    @Column(name="user_s_lastname", nullable = false)
    private String lastName;

    @Column(name="user_s_username", nullable = false, unique = true)
    private String username;

    @Column(name="user_n_active", nullable = false, columnDefinition = "smallint default 1")
    private boolean active;

    @Column(name="user_s_email", nullable = false, unique = true)
    private String email;

    @Column(name="user_s_password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_authorities", joinColumns = {
            @JoinColumn(name = "user_authority_user_n_id", referencedColumnName = "user_n_id", foreignKey = @ForeignKey(name = "FK_USERAUTHORITY_USER"))}, inverseJoinColumns = {
            @JoinColumn(name = "user_authority_authority_n_id", referencedColumnName = "authority_n_id", foreignKey = @ForeignKey(name = "FK_USERAUTHORITY_AUTHORITY"))})
    private Set<Authority> authorities;


}
