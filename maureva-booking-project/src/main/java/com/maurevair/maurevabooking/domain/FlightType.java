package com.maurevair.maurevabooking.domain;

public enum FlightType {
    ARRIVAL, DEPARTURE
}
