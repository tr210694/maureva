package com.maurevair.maurevabooking.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "airport")
@NoArgsConstructor
public class Airport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "airport_n_id")
    private Long id;

    @Column(name = "airport_s_code", length = 3, nullable = false, unique = true)
    @CsvBindByName
    private String airportCode;

    @Column(name = "airport_s_country", length = 50, nullable = false)
    @CsvBindByName
    private String country;

    @Column(name = "airport_s_region", length = 50, nullable = false)
    @CsvBindByName
    private String region;

    @Column(name = "airport_s_city", length = 50, nullable = false)
    @CsvBindByName
    private String city;

    public Airport(Long id,String airportCode, String country, String region, String city) {
        this.id = id;
        this.airportCode = airportCode;
        this.country = country;
        this.region = region;
        this.city = city;
    }
}
