package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.repository.AirportRepository;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    @Autowired
    private AirportRepository airportRepository;

    private static final Logger log = LoggerFactory.getLogger(AirportServiceImpl.class);


    @Override
    public List<Airport> uploadAirportCsvFile(MultipartFile multipartFile) {
        try (Reader reader = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()))) {
            CsvToBean<Airport> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(Airport.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            List<Airport> airports = csvToBean.parse();

            return airportRepository.saveAll(airports);
        } catch (Exception ex) {
            log.error("An error occurred while processing the CSV file." , ex);
            return new ArrayList<>();
        }

    }

    @Override
    public List<Airport> loadAllAirports() {
        return airportRepository.findAll();
    }

    @Override
    public Airport loadAirportByAirportCode(String airportCode) {
        return airportRepository.findAirportByAirportCode(airportCode);
    }
}