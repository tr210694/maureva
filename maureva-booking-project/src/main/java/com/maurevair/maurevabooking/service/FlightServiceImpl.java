package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.exception.AirportNotFoundException;
import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import com.maurevair.maurevabooking.model.Flights;
import com.maurevair.maurevabooking.repository.AirportRepository;
import com.maurevair.maurevabooking.repository.CustomFlightRepository;
import com.maurevair.maurevabooking.repository.FlightRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.ArrayList;
import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {
    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private CustomFlightRepository customFlightRepository;

    private static final Logger log = LoggerFactory.getLogger(FlightServiceImpl.class);

    @Override
    public List<Flight> uploadFileXmlFile(MultipartFile multipartFile) throws Exception {
        List<Flight> flightsTobeSaved = new ArrayList<>();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Flights.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Flights flights = (Flights) unmarshaller.unmarshal(multipartFile.getInputStream());
            for (Flight flight : flights.getFlights()) {
                Airport originAirport = airportRepository.findAirportByAirportCode(flight.getOriginAirportCode());
                Airport destinationAirport = airportRepository.findAirportByAirportCode(flight.getDestinationAirportCode());
                if (originAirport == null) {
                    throw new AirportNotFoundException("Airport with airportCode  " + flight.getOriginAirportCode() + " is not present");
                }
                if (destinationAirport == null) {
                    throw new AirportNotFoundException("Airport with airportCode  " + flight.getDestinationAirportCode() + " is not present");
                }
                flight.getBookingInfos().forEach(bookingInfo -> {
                    bookingInfo.setFlight(flight);
                });
                flight.setDestination(destinationAirport);
                flight.setOrigin(originAirport);
                flightsTobeSaved.add(flight);
            }


        } catch (AirportNotFoundException exception) {
            log.error(String.valueOf(exception));
        }
        return flightRepository.saveAll(flightsTobeSaved);
    }


    @Override
    public List<Flight> loadFlightsBetweenOriginAndDestinationAirport(Long originAirportId, Long destinationAirportId) {
        return flightRepository.findAllFlightsBetweenOriginAndDestination(originAirportId, destinationAirportId);
    }

    @Override
    public List<AvailableFlightBookingDto> loadAvailableBookingsForFlight(String flightId) {
        return customFlightRepository.findAvailableSeatsForFlightByCabinClass(flightId);
    }


	@Override
	public List<Flight> loadAllAvailableFlights() {
		return flightRepository.loadAllAvailableFlights();
	}

}
