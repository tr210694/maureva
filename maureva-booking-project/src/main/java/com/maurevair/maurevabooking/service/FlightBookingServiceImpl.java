package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.repository.FlightBookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FlightBookingServiceImpl implements FlightBookingService {

    @Autowired
    private FlightBookingRepository flightBookingRepository ;

    @Override
    public FlightBooking saveFlightBooking(FlightBooking flightBooking) {
        return flightBookingRepository.save(flightBooking);
    }


}
