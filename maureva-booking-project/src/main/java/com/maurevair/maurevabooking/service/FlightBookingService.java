package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.FlightBooking;

public interface FlightBookingService {
    FlightBooking saveFlightBooking(FlightBooking flightBooking);
}