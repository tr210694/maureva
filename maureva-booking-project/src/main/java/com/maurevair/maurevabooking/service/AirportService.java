package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.Airport;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface AirportService {
    List<Airport> uploadAirportCsvFile(MultipartFile multipartFile);
    List<Airport> loadAllAirports();
    Airport loadAirportByAirportCode(String airportCode);
}
