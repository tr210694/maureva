package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import com.maurevair.maurevabooking.domain.Flight;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface FlightService {

    List<Flight> uploadFileXmlFile(MultipartFile multipartFile) throws Exception;
    List<Flight> loadFlightsBetweenOriginAndDestinationAirport(Long originAirportId , Long destinationAirportId);
    List<AvailableFlightBookingDto> loadAvailableBookingsForFlight(String flightId);
    List<Flight> loadAllAvailableFlights();
}
