package com.maurevair.maurevabooking.model;

import com.maurevair.maurevabooking.domain.CabinClass;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvailableFlightBookingDto {
    private String flightId;
    private CabinClass cabinClass;
    private Long availableBookings;

    public AvailableFlightBookingDto(String flightId, CabinClass cabinClass, Long availableBookings) {
        this.flightId = flightId;
        this.cabinClass = cabinClass;
        this.availableBookings = availableBookings;
    }
}
