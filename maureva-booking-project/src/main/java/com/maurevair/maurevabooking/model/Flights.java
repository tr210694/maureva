package com.maurevair.maurevabooking.model;

import com.maurevair.maurevabooking.domain.Flight;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "flights")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Flights {
    @XmlElement(name = "flight")
    private List<Flight> flights;
}
