package com.maurevair.maurevabooking.adapter;

import com.maurevair.maurevabooking.domain.CabinClass;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CabinClassAdapter extends  XmlAdapter<String, CabinClass> {

    @Override
    public CabinClass unmarshal(String s) throws Exception {
        return CabinClass.valueOf(s.toUpperCase());
    }

    @Override
    public String marshal(CabinClass cabinClass) throws Exception {
        return cabinClass.name();
    }
}
