package com.maurevair.maurevabooking.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeAdapter extends  XmlAdapter<String, ZonedDateTime> {

    @Override
    public ZonedDateTime unmarshal(String s) throws Exception {
        return ZonedDateTime.parse(s);
    }

    @Override
    public String marshal(ZonedDateTime zonedDateTime) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/mm/dd - HH:mm:ss Z");
        System.out.println(zonedDateTime.format(formatter));
        return zonedDateTime.format(formatter);
    }
}
