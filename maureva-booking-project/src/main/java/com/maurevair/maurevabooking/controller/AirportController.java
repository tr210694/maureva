package com.maurevair.maurevabooking.controller;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class AirportController {
    @Autowired
    private AirportService airportService;

    @GetMapping("/airports")
    public ResponseEntity<List<Airport>> loadAllAirports() {
        List<Airport> airports = airportService.loadAllAirports();
        return new ResponseEntity<>(airports, HttpStatus.OK);
    }

    @GetMapping("/airports/{airportCode}")
    public ResponseEntity<Airport> loadAirportByName(@PathVariable("airportCode")String airportCode) {
        Airport airport = airportService.loadAirportByAirportCode(airportCode);
        return new ResponseEntity<>(airport, HttpStatus.OK);
    }

    @PostMapping("/airports")
    public ResponseEntity<List<Airport>> uploadAirportFile(@RequestParam("file") MultipartFile file){
        try {
            List<Airport> airports = airportService.uploadAirportCsvFile(file);
            return ResponseEntity.status(HttpStatus.OK).body(airports);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.valueOf(500)).build();
        }
    }

}
