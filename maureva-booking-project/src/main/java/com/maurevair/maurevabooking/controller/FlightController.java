package com.maurevair.maurevabooking.controller;

import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class FlightController {
    @Autowired
    private FlightService flightService;


    @PostMapping("/flights")
    public ResponseEntity<List<Flight>> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        List<Flight> flights = flightService.uploadFileXmlFile(file);
        return ResponseEntity.status(HttpStatus.OK).body(flights);
    }
    
    @GetMapping("/all-flights")
    public List<Flight> loadAllAvailableFlights() {
         return flightService.loadAllAvailableFlights();
    }

    @GetMapping("/flights/{originAirportId}/{destinationAirportId}")
    public ResponseEntity<List<Flight>>getFlightByOriginAndDestinationAirport(@PathVariable("originAirportId") Long originAirportId
            , @PathVariable("destinationAirportId") Long destinationAirportId){
        List<Flight> flights = flightService.loadFlightsBetweenOriginAndDestinationAirport(originAirportId, destinationAirportId);
        return ResponseEntity.status(HttpStatus.OK).body(flights);
    }

    @GetMapping("/flights/availablep-seats/{flightId}")
    public ResponseEntity<List<AvailableFlightBookingDto>>getAvailableSeatsForFlightByCabinClass(@PathVariable("flightId")String flightId){
        List<AvailableFlightBookingDto> availableFlightBookingDtos = flightService.loadAvailableBookingsForFlight(flightId);
        return ResponseEntity.status(HttpStatus.OK).body(availableFlightBookingDtos);
    }
}
