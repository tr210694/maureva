package com.maurevair.maurevabooking.controller;

import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.service.FlightBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlightBookingController {

    @Autowired
    private FlightBookingService flightBookingService ;

    @PostMapping("/flight-bookings")
    public ResponseEntity<FlightBooking> bookFlight(@RequestBody FlightBooking flightBooking){
        FlightBooking savedBooking = flightBookingService.saveFlightBooking(flightBooking);
        return new ResponseEntity<>(savedBooking, HttpStatus.OK);
    }
}
