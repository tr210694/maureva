package com.maurevair.maurevabooking.repository;


import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import com.maurevair.maurevabooking.config.AppConfig;
import com.maurevair.maurevabooking.domain.CabinClass;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.domain.User;

@DataJpaTest
@ActiveProfiles("test")
@Import(AppConfig.class)
@Transactional
@Sql("/insertData.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FlightBookingRepositoryTest {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private FlightBookingRepository flightBookingRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void saveFlightBooking_shouldSaveValidRecord() {
        Flight flight = flightRepository.findById("sQTWMC3R2BKAzkvFCAAAAA==").get();
        User user = userRepository.getUserByUsername("ter");
        FlightBooking flightBooking = createFlightBooking(flight, user);

        flightBookingRepository.save(flightBooking);

        List<FlightBooking> allFlightBookings = flightBookingRepository.findAll();

        Assertions.assertThat(allFlightBookings).hasSize(2);
        Assertions.assertThat(allFlightBookings.get(1).getBookingReference()).isEqualTo("Booking ref 12345");
    }

    private FlightBooking createFlightBooking(Flight flight, User user) {
        FlightBooking flightBooking = new FlightBooking();
        flightBooking.setFlight(flight);
        flightBooking.setBookedBy(user);
        flightBooking.setPhoneNumber("+23052505565");
        flightBooking.setPassportNumber("1929075554");
        flightBooking.setLastName(user.getLastName());
        flightBooking.setFirstName(user.getFirstName());
        flightBooking.setEmail(user.getEmail());
        flightBooking.setCabinClass(CabinClass.ECONOMY);
        flightBooking.setBookingReference("Booking ref 12345");
        return flightBooking;
    }


}
