package com.maurevair.maurevabooking.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.domain.BookingInfo;
import com.maurevair.maurevabooking.domain.CabinClass;
import com.maurevair.maurevabooking.domain.Flight;

@DataJpaTest
@Transactional
@ActiveProfiles("test")

public class FlightRepositoryTest {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private AirportRepository airportRepository;

    private Flight flight;
    private Airport originAirport;
    private Airport destinationAirport;

    @BeforeEach
    public void setUp() {
        originAirport = createOriginAirport();
        destinationAirport = createDestinationAirport();
        flight = createAndSaveFlight("ACMkFvAAWT2A=sQBz=KAACR3", originAirport, destinationAirport);
    }


    @Test
    public void saveFlight_shouldSaveFlightRecord() {
        Flight newFlight = createAndSaveFlight("sQTWMC3R2BKAzkvFCAAAAA==", originAirport, destinationAirport);
        List<Flight> allFlights = flightRepository.findAll();
        assertThat(allFlights).isNotEmpty();
        assertThat(allFlights).hasSize(2);
        assertThat(allFlights).containsExactly(flight, newFlight);
    }

    @Test
    public void findAllFlightsBetweenOriginAndDestination_shouldReturnListOfLightsFromOriginAndDestination(){
        List<Flight> allFlightsBetweenOriginAndDestination = flightRepository.findAllFlightsBetweenOriginAndDestination(originAirport.getId(), destinationAirport.getId());
        assertThat(allFlightsBetweenOriginAndDestination).isNotEmpty();
        assertThat(allFlightsBetweenOriginAndDestination).hasSize(1);
        assertThat(allFlightsBetweenOriginAndDestination).containsExactly(flight);
    }

    private Flight createAndSaveFlight(String flightId, Airport originAirport, Airport destinationAirport) {
        Flight newFlight = createFlight(flightId);
        newFlight.setOrigin(originAirport);
        newFlight.setDestination(destinationAirport);
        List<BookingInfo> bookingInfoList = createBookingInfoList(newFlight);
        newFlight.setBookingInfos(bookingInfoList);
        return flightRepository.save(newFlight);
    }


    private List<BookingInfo> createBookingInfoList(Flight flight) {
        BookingInfo bookingInfoFirstClass = new BookingInfo();
        bookingInfoFirstClass.setFlight(flight);
        bookingInfoFirstClass.setCabinClass(CabinClass.FIRST);
        bookingInfoFirstClass.setBookingCounts(10);

        BookingInfo bookingInfoPremiumEconomy = new BookingInfo();
        bookingInfoPremiumEconomy.setFlight(flight);
        bookingInfoPremiumEconomy.setCabinClass(CabinClass.PREMIUMECONOMY);
        bookingInfoPremiumEconomy.setBookingCounts(50);

        BookingInfo bookingInfoEconomy = new BookingInfo();
        bookingInfoEconomy.setFlight(flight);
        bookingInfoEconomy.setCabinClass(CabinClass.ECONOMY);
        bookingInfoEconomy.setBookingCounts(60);

        List<BookingInfo> bookingInfoList = new ArrayList<>();


        bookingInfoList.add(bookingInfoEconomy);
        bookingInfoList.add(bookingInfoFirstClass);
        bookingInfoList.add(bookingInfoPremiumEconomy);
        return bookingInfoList;
    }

    private Airport createDestinationAirport() {
        Airport destinationAirport = new Airport();
        destinationAirport.setAirportCode("CDG");
        destinationAirport.setCountry("France");
        destinationAirport.setRegion("Paris");
        destinationAirport.setCity("Charles de Gaulle");
        return airportRepository.save(originAirport);
    }

    private Airport createOriginAirport() {
        Airport originAirport = new Airport();
        originAirport.setAirportCode("MRU");
        originAirport.setCountry("Mauritius");
        originAirport.setRegion("Grand Port");
        originAirport.setCity("Plaine Magnien");
        return airportRepository.save(originAirport);
    }

    private Flight createFlight(String flightId) {
        Flight flight = new Flight();
        flight.setFlightNumber("1234");
        flight.setId(flightId);
        flight.setArrivalTime(ZonedDateTime.of(LocalDateTime.of(2022, 05, 01, 12, 8), ZoneId.of("Indian/Mauritius")));
        flight.setDepartureTime(ZonedDateTime.of(LocalDateTime.of(2022, 05, 01, 23, 26), ZoneId.of("Europe/Paris")));
        flight.setCarrier("MRV");
        return flight;
    }


}
