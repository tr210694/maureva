package com.maurevair.maurevabooking.repository;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import com.maurevair.maurevabooking.config.AppConfig;
import com.maurevair.maurevabooking.domain.Airport;

@DataJpaTest()
@Transactional
@ActiveProfiles("test")
@Import(AppConfig.class)
public class AirportsRepositoryTest {

    @Autowired
    private AirportRepository airportRepository ;
    private Airport airport ;

    @BeforeEach
    public void setUp(){
        airport = createAirport();

    }

    @Test
    public void findAll_shouldReturnAllAirportsSaved(){
        List<Airport> allAirports = airportRepository.findAll();
        assertThat(allAirports).isNotEmpty();
        assertThat(allAirports).hasSize(1);
        assertThat(allAirports).containsExactly(airport);

    }

    @Test
    public void loadAirportsByAirportCode_shouldReturnAirportWithAirportCode(){
       Airport airportByCode= airportRepository.findAirportByAirportCode("MUR");
        assertThat(airportByCode).isNotNull();
        assertThat(airportByCode).isEqualTo(airport);

    }

    @Test
    public void loadAirportsByAirportCodeAirport_shouldReturnNullWhenAirportWithCodeNotFound(){
        Airport airportByCode= airportRepository.findAirportByAirportCode("CDG");
        assertThat(airportByCode).isNull();

    }

    @Test
    public void saveAllAirport_shouldsSuccessllySaveAirport(){
        Airport newAirport = new Airport();
        newAirport.setAirportCode("CDG");
        newAirport.setCity("Charles de Gaulle");
        newAirport.setRegion("Paris");
        newAirport.setCountry("France");
        List<Airport> airportsTobeSaved = new ArrayList<>();
        airportsTobeSaved.add(newAirport);

        airportsTobeSaved = airportRepository.saveAll(airportsTobeSaved);

        List<Airport> allAirports = airportRepository.findAll();
        assertThat(allAirports).isNotEmpty();
        assertThat(allAirports).hasSize(2);
        assertThat(allAirports).containsExactly(airport,newAirport);

    }

    @Test()
    public void testSaveAirportWithExistingAirportCode(){
        Airport newAirport = new Airport();
        newAirport.setAirportCode("CDG");
        newAirport.setCity("Charles de Gaulle");
        newAirport.setRegion("Paris");
        newAirport.setCountry("France");

        Airport newAirport2 = new Airport();
        newAirport2.setAirportCode("CDG");
        newAirport2.setCity("Charles de Gaulle");
        newAirport2.setRegion("Paris");
        newAirport2.setCountry("France");

        List<Airport> airportsTobeSaved = new ArrayList<>();
        airportsTobeSaved.add(newAirport);
        airportsTobeSaved.add(newAirport2);

        assertThrows(DataIntegrityViolationException.class, () ->  airportRepository.saveAll(airportsTobeSaved));


    }


    private Airport createAirport() {
        Airport airport = new Airport();
        airport.setAirportCode("MUR");
        airport.setCity("Plaine Magnien");
        airport.setRegion("Grand port");
        airport.setCountry("Mauritius");
        return airportRepository.save(airport);
    }
}
