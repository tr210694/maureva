package com.maurevair.maurevabooking.repository;

import com.maurevair.maurevabooking.config.AppConfig;
import com.maurevair.maurevabooking.domain.CabinClass;
import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.annotation.DirtiesContext.*;


@DataJpaTest
@ActiveProfiles("test")
@Import(AppConfig.class)
@Transactional
@Sql("/insertData.sql")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class CustomFlightRepositoryTest {

    @Autowired
    private CustomFlightRepository flightRepository;
    @Test
    public void findAvailableSeatsForFlightByCabinClass_shouldReturnAvailableSeatsForSpecificFlightByClass() {
        List<AvailableFlightBookingDto> availableSeats = flightRepository.findAvailableSeatsForFlightByCabinClass("sQTWMC3R2BKAzkvFCAAAAA==");

        assertEquals(3, availableSeats.size());

        AvailableFlightBookingDto availableSeatsForEconomy = availableSeats.get(0);
        assertEquals("sQTWMC3R2BKAzkvFCAAAAA==", availableSeatsForEconomy.getFlightId());
        assertEquals(CabinClass.ECONOMY, availableSeatsForEconomy.getCabinClass());
        assertEquals(49L, availableSeatsForEconomy.getAvailableBookings());

        AvailableFlightBookingDto availableSeatsForFirst = availableSeats.get(1);
        assertEquals("sQTWMC3R2BKAzkvFCAAAAA==", availableSeatsForFirst.getFlightId());
        assertEquals(CabinClass.FIRST, availableSeatsForFirst.getCabinClass());
        assertEquals(20L, availableSeatsForFirst.getAvailableBookings());

        AvailableFlightBookingDto availableSeatsForPremium = availableSeats.get(2);
        assertEquals("sQTWMC3R2BKAzkvFCAAAAA==", availableSeatsForPremium.getFlightId());
        assertEquals(CabinClass.PREMIUMECONOMY, availableSeatsForPremium.getCabinClass());
        assertEquals(30L, availableSeatsForPremium.getAvailableBookings());
    }

    @Test
    public void findAvailableSeatsForFlightByCabinClass_shouldReturnEmptyListWhenNoFlightIsFound() {
        List<AvailableFlightBookingDto> availableSeats = flightRepository.findAvailableSeatsForFlightByCabinClass("flightId2663132");
        assertTrue(availableSeats.isEmpty());
    }


}