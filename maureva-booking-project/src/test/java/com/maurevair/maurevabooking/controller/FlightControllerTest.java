package com.maurevair.maurevabooking.controller;

import com.maurevair.maurevabooking.domain.CabinClass;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import com.maurevair.maurevabooking.service.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightControllerTest {

    @Mock
    private FlightService flightService;

    @InjectMocks
    private FlightController flightController;

    @Test
    void uploadFile_shouldReturnOK() throws Exception {
        List<Flight> expectedFlights = new ArrayList<>();
        expectedFlights.add(new Flight());

        String content = "<flights><flight><id>FL001</id><name>Test Flight</name></flight></flights>";
        InputStream inputStream = new ByteArrayInputStream(content.getBytes());
        MultipartFile mockFile = new MockMultipartFile("file", "test.xml", "text/xml", inputStream);

        when(flightService.uploadFileXmlFile(any(MultipartFile.class))).thenReturn(expectedFlights);

        ResponseEntity<List<Flight>> response = flightController.uploadFile(mockFile);
        List<Flight> actualFlights = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedFlights, actualFlights);
    }

    @Test
    public void getFlightByOriginAndDestinationAirport_shouldReturnListOfFlight() {
        Flight flight1 = new Flight();
        Flight flight2 = new Flight();
        List<Flight> flights = Arrays.asList(flight1, flight2);
        when(flightService.loadFlightsBetweenOriginAndDestinationAirport(anyLong(), anyLong())).thenReturn(flights);

        ResponseEntity<List<Flight>> responseEntity = flightController.getFlightByOriginAndDestinationAirport(1L, 2L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(flights, responseEntity.getBody());
    }

    @Test
    public void getAvailableSeatsForFlightByCabinClass_shouldReturnAvailableSeatForFlightByCabinClass() {
        String flightId = "flight1265sds";
        AvailableFlightBookingDto dto1 = new AvailableFlightBookingDto("flight1265sds", CabinClass.FIRST, 11L);
        AvailableFlightBookingDto dto2 = new AvailableFlightBookingDto("flight1265sds", CabinClass.ECONOMY, 5L);
        List<AvailableFlightBookingDto> dtos = Arrays.asList(dto1, dto2);
        when(flightService.loadAvailableBookingsForFlight(anyString())).thenReturn(dtos);

        ResponseEntity<List<AvailableFlightBookingDto>> responseEntity = flightController.getAvailableSeatsForFlightByCabinClass(flightId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(dtos, responseEntity.getBody());
    }
}
