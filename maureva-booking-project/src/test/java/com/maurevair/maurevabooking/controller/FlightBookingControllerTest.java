package com.maurevair.maurevabooking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.service.FlightBookingService;

@ExtendWith(MockitoExtension.class)
public class FlightBookingControllerTest {

    @Mock
    private FlightBookingService flightBookingService;

    @InjectMocks
    private FlightBookingController flightBookingController;

    @Test
    void bookFlight_shouldSaveFlightBookingAndReturnSavedBooking() {
        FlightBooking flightBooking = new FlightBooking();
        when(flightBookingService.saveFlightBooking(any(FlightBooking.class))).thenReturn(flightBooking);

        ResponseEntity<FlightBooking> response = flightBookingController.bookFlight(flightBooking);

        verify(flightBookingService).saveFlightBooking(any(FlightBooking.class));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(flightBooking, response.getBody());
    }
}
