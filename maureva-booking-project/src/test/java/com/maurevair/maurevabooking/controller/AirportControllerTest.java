package com.maurevair.maurevabooking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.service.AirportService;

@ExtendWith(MockitoExtension.class)
public class AirportControllerTest {

    @Mock
    private AirportService airportService;

    @InjectMocks
    private AirportController airportController;

    @Test
    void loadAllAirport_shouldReturnListOfAllAirports() {
        List<Airport> expectedAirports = new ArrayList<>();
        expectedAirports.add(new Airport(1L, "MRU", "Maurice", "Grand Port", "Plaine Magnien"));
        expectedAirports.add(new Airport(2L, "CDG", "France", "Paris", "Charles de gaull"));
        expectedAirports.add(new Airport(3L, "ORY", "France", "Paris", "Orly"));

        when(airportService.loadAllAirports()).thenReturn(expectedAirports);

        ResponseEntity<List<Airport>> response = airportController.loadAllAirports();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedAirports, response.getBody());
    }

    @Test
    void loadAirportByName_shouldReturnOneAirportBasedOnInputName() {

        Airport expectedAirport = new Airport(5L, "DXB", "United Arabs Emirate", "Abu Dabi", "DUBAI");

        when(airportService.loadAirportByAirportCode(anyString())).thenReturn(expectedAirport);

        ResponseEntity<Airport> responseEntity = airportController.loadAirportByName("DXB");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedAirport, responseEntity.getBody());
    }

    @Test
    void uploadAirportFile_shouldParseAndSaveAirports() {
        String fileName = "airports.csv";
        String csvData = "country,region,city,airportCode\n" +
                "Maurice,Grand Port,Plaine Magnien,MRU\n" +
                "France,Paris,Charles de gaulle,CDG\n" +
                "France,Paris,Orly,ORY\n" ;
        byte[] fileContent = csvData.getBytes();
        MockMultipartFile file = new MockMultipartFile(fileName, fileName, MediaType.TEXT_PLAIN_VALUE, fileContent);
        List<Airport> expectedAirports = new ArrayList<>();
        expectedAirports.add(new Airport(1L, "MRU", "Maurice", "Grand Port", "Plaine Magnien"));
        expectedAirports.add(new Airport(2L, "CDG", "France", "Paris", "Charles de gaull"));
        expectedAirports.add(new Airport(3L, "ORY","France", "Paris", "Orly"));


        when(airportService.uploadAirportCsvFile(file)).thenReturn(expectedAirports);

        ResponseEntity<List<Airport>> responseEntity = airportController.uploadAirportFile(file);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedAirports, responseEntity.getBody());
    }

    @Test
    void uploadAirportFile_shouldReturnInternalServerErrorWhenFileIsInvalid() {

        byte[] fileContent = "Test file content".getBytes();
        MultipartFile file = new MockMultipartFile("file", "test.txt", "text/plain", fileContent);

        when(airportService.uploadAirportCsvFile(any(MockMultipartFile.class))).thenThrow(new RuntimeException());

        ResponseEntity<List<Airport>> responseEntity = airportController.uploadAirportFile(file);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertNull(responseEntity.getBody());
    }

}