package com.maurevair.maurevabooking.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.repository.AirportRepository;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

@ExtendWith(MockitoExtension.class)
public class AirportServiceUnitTest {

    @Mock
    private AirportRepository airportRepository;

    @InjectMocks
    private AirportServiceImpl airportService;

    @Test
    public void uploadAirportCsvFile_ShouldSaveAirports_WhenFileIsValid() throws IOException, CsvValidationException {
        String csvData = "country,region,city,airportCode\n" +
                "Maurice,Grand Port,Plaine Magnien,MRU\n" +
                "France,Paris,Charles de gaulle,CDG\n" +
                "France,Paris,Orly,ORY\n" ;
        MockMultipartFile csvFile = new MockMultipartFile("data.csv", csvData.getBytes());
        CSVReader reader = new CSVReader(new StringReader(csvData));
        List<Airport> expectedAirports = new ArrayList<>();
        expectedAirports.add(new Airport(1L,"MRU", "Maurice","Grand Port","Plaine Magnien"));
        expectedAirports.add(new Airport(2L, "CDG", "France", "Paris", "Charles de gaulle"));
        expectedAirports.add(new Airport(3L, "ORY","France", "Paris", "Orly"));

        when(airportRepository.saveAll(anyList())).thenReturn(expectedAirports);

        List<Airport> actualAirports = airportService.uploadAirportCsvFile(csvFile);

        assertEquals(expectedAirports, actualAirports);
        verify(airportRepository, times(1)).saveAll(anyList());
    }

    @Test
    public void uploadAirportCsvFile_ShouldReturnEmptyList_WhenFileIsInvalid() {
        String invalidCsvData = "country,region,city,airportCode\n" +
                "Maurice,Grand Port,Plaine Magnien,Test,MRU\n" +
                "France,Paris,Charles de gaulle,CDG\n" +
                "France,Paris,Orly,ORY\n" ;
        MockMultipartFile invalidCsvFile = new MockMultipartFile("data.csv", invalidCsvData.getBytes());

        List<Airport> actualAirports = airportService.uploadAirportCsvFile(invalidCsvFile);

        assertTrue(actualAirports.isEmpty());
    }

    @Test
    public void loadAllAirports_ShouldReturnAllAirports() {
        List<Airport> expectedAirports = new ArrayList<>();
        expectedAirports.add(new Airport(1L,"MRU", "Maurice","Grand Port","Plaine Magnien"));
        expectedAirports.add(new Airport(2L, "CDG", "France", "Paris", "Charles de gaulle"));
        expectedAirports.add(new Airport(3L, "ORY","France", "Paris", "Orly"));

        when(airportRepository.findAll()).thenReturn(expectedAirports);

        List<Airport> actualAirports = airportService.loadAllAirports();

        assertEquals(expectedAirports, actualAirports);
        verify(airportRepository, times(1)).findAll();
    }

    @Test
    void testLoadAirportByAirportCode() {
        String airportCode = "DXB";
        Airport expectedAirport = new Airport(5L,"DXB", "United Arabs Emirate", "Abu Dabi", "DUBAI");

        when(airportRepository.findAirportByAirportCode(airportCode)).thenReturn(expectedAirport);

        Airport actualAirport = airportService.loadAirportByAirportCode(airportCode);

        assertEquals(expectedAirport, actualAirport);
        verify(airportRepository, times(1)).findAirportByAirportCode(airportCode);
    }
}

