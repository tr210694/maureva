package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.FlightBooking;
import com.maurevair.maurevabooking.repository.FlightBookingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FlightBookingServiceUnitTest {
    @Mock
    private FlightBookingRepository flightBookingRepository;

    @InjectMocks
    private FlightBookingServiceImpl flightBookingService;

    @Test
    public void saveFlightBooking_shouldSaveAndReturnFlightBooking() {
        FlightBooking flightBooking = new FlightBooking();
        when(flightBookingRepository.save(any(FlightBooking.class))).thenReturn(flightBooking);

        FlightBooking savedFlightBooking = flightBookingService.saveFlightBooking(flightBooking);

        verify(flightBookingRepository, times(1)).save(any(FlightBooking.class));
        assertNotNull(savedFlightBooking);
        assertEquals(flightBooking, savedFlightBooking);
    }
}