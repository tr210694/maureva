package com.maurevair.maurevabooking.service;

import com.maurevair.maurevabooking.domain.Airport;
import com.maurevair.maurevabooking.domain.BookingInfo;
import com.maurevair.maurevabooking.domain.CabinClass;
import com.maurevair.maurevabooking.domain.Flight;
import com.maurevair.maurevabooking.model.AvailableFlightBookingDto;
import com.maurevair.maurevabooking.model.Flights;
import com.maurevair.maurevabooking.repository.AirportRepository;
import com.maurevair.maurevabooking.repository.CustomFlightRepository;
import com.maurevair.maurevabooking.repository.FlightRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FlightServiceUnitTest {

    @Mock
    private AirportRepository airportRepository;

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private CustomFlightRepository customFlightRepository;


    @InjectMocks
    private FlightServiceImpl flightService;

    @Test
    void testUploadFileXmlFile() throws Exception {
        InputStream xmlFile = getClass().getResourceAsStream("/test-flight-data.xml");
        MultipartFile multipartFile = new MockMultipartFile("test-file.xml", xmlFile);
        List<Flight> expectedFlights = Arrays.asList(this.createFlight());

        when(airportRepository.findAirportByAirportCode(anyString())).thenReturn(new Airport());
        when(flightRepository.saveAll(anyIterable())).thenReturn(expectedFlights);
        List<Flight> actualFlights = flightService.uploadFileXmlFile(multipartFile);

        verify(airportRepository, times(2)).findAirportByAirportCode(anyString());
        verify(flightRepository).saveAll(anyIterable());
        Assertions.assertThat(actualFlights).hasSize(1);
        Assertions.assertThat(actualFlights.get(0).getId()).isEqualTo(expectedFlights.get(0).getId());
    }


    @Test
    void testLoadFlightsBetweenOriginAndDestinationAirport() {
        List<Flight> expectedFlights = new ArrayList<>();
        expectedFlights.add(new Flight());

        when(flightRepository.findAllFlightsBetweenOriginAndDestination(anyLong(), anyLong()))
                .thenReturn(expectedFlights);

        List<Flight> actualFlights = flightService.loadFlightsBetweenOriginAndDestinationAirport(1L, 2L);

        assertEquals(expectedFlights, actualFlights);
    }

    @Test
    void testLoadAvailableBookingsForFlight() {
        List<AvailableFlightBookingDto> expectedBookings = new ArrayList<>();
        AvailableFlightBookingDto premium = new AvailableFlightBookingDto("flightId12662", CabinClass.PREMIUMECONOMY, 20L);
        AvailableFlightBookingDto first = new AvailableFlightBookingDto("flightId12662", CabinClass.FIRST, 10L);
        AvailableFlightBookingDto economy = new AvailableFlightBookingDto("flightId12662", CabinClass.ECONOMY, 55L);
        expectedBookings.add(premium);
        expectedBookings.add(first);
        expectedBookings.add(economy);

        when(customFlightRepository.findAvailableSeatsForFlightByCabinClass(anyString()))
                .thenReturn(expectedBookings);

        List<AvailableFlightBookingDto> actualBookings = flightService.loadAvailableBookingsForFlight("flightId12662");

        assertEquals(expectedBookings, actualBookings);
    }


    private Flight createFlight() {
        Flight flight = new Flight();
        flight.setOrigin(createOriginAirport());
        flight.setDestination(createDestinationAirport());
        flight.setCarrier("MRV");
        flight.setId("sQTWMC3R2BKAzkvFCAAAAA==");
        flight.setArrivalTime(ZonedDateTime.of(LocalDateTime.of(2022, 05, 01, 12, 8), ZoneId.of("Indian/Mauritius")));
        flight.setDepartureTime(ZonedDateTime.of(LocalDateTime.of(2022, 05, 01, 23, 26), ZoneId.of("Europe/Paris")));
        flight.setBookingInfos(this.createBookingInfoList());
        return flight;
    }

    private List<BookingInfo> createBookingInfoList() {
        List<BookingInfo> bookingInfoList = new ArrayList<>();
        BookingInfo bookingInfoForFirst = new BookingInfo();
        bookingInfoForFirst.setBookingCounts(10);
        bookingInfoForFirst.setCabinClass(CabinClass.FIRST);

        BookingInfo bookingInfoForPremiumEconomy = new BookingInfo();
        bookingInfoForPremiumEconomy.setBookingCounts(50);
        bookingInfoForPremiumEconomy.setCabinClass(CabinClass.PREMIUMECONOMY);

        BookingInfo bookingInfoForEconomy = new BookingInfo();
        bookingInfoForEconomy.setBookingCounts(60);
        bookingInfoForEconomy.setCabinClass(CabinClass.ECONOMY);

        bookingInfoList.add(bookingInfoForFirst);
        bookingInfoList.add(bookingInfoForPremiumEconomy);
        bookingInfoList.add(bookingInfoForEconomy);
        return bookingInfoList;
    }

    private Airport createDestinationAirport() {
        Airport destinationAirport = new Airport();
        destinationAirport.setAirportCode("CDG");
        destinationAirport.setCountry("France");
        destinationAirport.setRegion("Paris");
        destinationAirport.setCity("Charles de Gaulle");
        return destinationAirport;
    }

    private Airport createOriginAirport() {
        Airport originAirport = new Airport();
        originAirport.setAirportCode("MRU");
        originAirport.setCountry("Mauritius");
        originAirport.setRegion("Grand Port");
        originAirport.setCity("Plaine Magnien");
        return originAirport;
    }
}