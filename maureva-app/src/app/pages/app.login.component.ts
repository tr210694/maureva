import { Token } from '@angular/compiler';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstant } from '../app-constant';
import { LoginDetails } from '../demo/domain/login-details';
import { AuthenticationService } from '../demo/service/authentication-service';

@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
})
export class AppLoginComponent {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  loginDetails: LoginDetails = new LoginDetails(); 

  ngOnInit(): void {
    this.authenticationService.getStorage().removeItem(AppConstant.MAUREVAIR_TOKEN);
  }

  doLogin(): void {
    const username: string = this.loginDetails.username.trim();
    const password: string = this.loginDetails.password;

    this.authenticationService.doLogin(username, password)
    .subscribe({
      next: () => {
        this.router.navigate(['/']);
        const savedToken: Token = JSON.parse(this.authenticationService.getStorage().getItem(AppConstant.MAUREVAIR_TOKEN));
        if (savedToken) {
          console.log("All good")
        }
      }
    })
  }

}
