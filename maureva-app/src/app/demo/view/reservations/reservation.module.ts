import { NgModule } from '@angular/core';


import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ChartModule } from 'primeng/chart';
import {TimelineModule} from 'primeng/timeline';
import {BadgeModule} from 'primeng/badge';
import { ReservationRoutingModule } from './reservation-routing.module';
import { ReservationComponent } from './reservation.component';
import { ButtonModule } from 'primeng/button';

@NgModule({
    imports: [
        ReservationRoutingModule,
        PanelModule,
        PanelMenuModule,
        ChartModule,
        TimelineModule,
        BadgeModule,
    ButtonModule],
    declarations: [ReservationComponent]
})
export class ReservationModule { }
