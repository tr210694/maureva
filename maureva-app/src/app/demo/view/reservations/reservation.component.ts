import {Component, OnInit} from '@angular/core';
import { BreadcrumbService } from 'src/app/app.breadcrumb.service';

@Component({
    templateUrl: './reservation.component.html'
})
export class ReservationComponent implements OnInit {

    

    constructor(private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: 'Reservation'}
        ]);
    }

    ngOnInit() {
      
    }

    getReservations() {
        
    }
    
}
