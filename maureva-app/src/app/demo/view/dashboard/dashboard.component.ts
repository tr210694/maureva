import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { BreadcrumbService } from 'src/app/app.breadcrumb.service';
import { BookingInfo } from 'src/app/demo/domain/booking';
import { Flight } from 'src/app/demo/domain/flight';
import { FlightService } from 'src/app/demo/service/flight-service';
import { AirportService } from '../../service/airport-service';


@Component({
    templateUrl: './dashboard.component.html',
    providers: [DatePipe]
})
export class DashboardComponent implements OnInit {

    flights: Flight[] = [];

    chartOptions: any;

    items: MenuItem[];

    constructor(private breadcrumbService: BreadcrumbService,  private flightService: FlightService, private airportService: AirportService) {
        this.breadcrumbService.setItems([
            { label: 'Dashboard' }
        ]);
    }

    ngOnInit() {
        this.getAllFlightDetails();
    }

    onUpload(event: any): void {
        console.log(event)
        this.getAllFlightDetails();
        console.log(event.currentFiles[0]);
        if (event.currentFiles[0].type.includes('xml')) {
            this.flightService.uploadFlightDetails(event.currentFiles[0])
            .subscribe(data => {
                this.flights = data;
                console.log(data);
            })
        } else {
            this.airportService.uploadAirportDetails(event.currentFiles[0])
            .subscribe(data => {
                console.log(data);
            })
        }
        
    }

    getAllFlightDetails(): void {
        this.flightService.loadAllFlights()
        .subscribe(data => {
            console.log(data)
            this.flights = data;          
        });
    }

    getTotalSeatsAvailable(bookingInfos: BookingInfo[]): number {
        if (bookingInfos){
            const seats: number = bookingInfos.reduce((acc, obj) => acc + obj.bookingCount, 0);
            return seats;
        }
        return 0;
    }
}
