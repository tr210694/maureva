import { NgModule } from '@angular/core';


import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ChartModule } from 'primeng/chart';
import {TimelineModule} from 'primeng/timeline';
import {BadgeModule} from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { TableModule } from 'primeng/table';
import { FileUpload, FileUploadModule } from 'primeng/fileupload';

@NgModule({
    imports: [
        DashboardRoutingModule,
        PanelModule,
        PanelMenuModule,
        ChartModule,
        TimelineModule,
        BadgeModule,
        TableModule,
        FileUploadModule,
    ButtonModule],
    declarations: [DashboardComponent]
})
export class DashboardModule { }
