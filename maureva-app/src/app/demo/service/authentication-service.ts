import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { AppConstant } from 'src/app/app-constant';
import { Token } from '../domain/token';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private API_OAUTH_TOKEN: string = environment.API_ENDPOINT + 'oauth/token';

    constructor(private http: HttpClient) { }
    user: any;

    doLogin(username: string, pass: string): Observable<any> {
       
        const url: string = this.API_OAUTH_TOKEN;
        console.log(url);
        let params = new URLSearchParams();
        params.append('username', username);
        params.append('password', pass);
        params.append('grant_type', 'password');
        return this.http
            .post(url, params.toString(), { headers: this.oauthHeader() })
            .pipe(map((res: Token) => {
                const tokenInfo = res;
                if (tokenInfo) {
                    this.saveToken(tokenInfo);
                }
            }));
    }

    doLogout() {
        const logoutUrl: string = environment.API_ENDPOINT + 'logout/info?initial=' + (this.getConnectedUser() === null ? null : this.getConnectedUser().username);
        return this.http
            .get(logoutUrl, { headers: this.contentTypeHeader() })
            .pipe(catchError(this.handleError));
    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred:', error);
        return throwError(() => error);
    }

    private oauthHeader(): HttpHeaders {
        let headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
        headers = headers.append('Authorization', 'Basic ' + btoa(AppConstant.CLIENT_ID));
        return headers;
    }

    saveToken(tokenInfo: Token): void {
        tokenInfo.strTokenGenerationDate = Date.now().toString();
        this.getStorage().setItem(AppConstant.MAUREVAIR_TOKEN, JSON.stringify(tokenInfo));
    }

    getTokenInfo(): Token {
        return JSON.parse(this.getStorage().getItem(AppConstant.MAUREVAIR_TOKEN));
    }


    getConnectedUser(): any {
        const tokenInfo: Token = JSON.parse(this.getStorage().getItem(AppConstant.MAUREVAIR_TOKEN));
        return tokenInfo ? tokenInfo.currentUser : null;
    }

    isLogged(): boolean {
        this.user = this.getConnectedUser();
        const isConnectedUser = this.getConnectedUser();
        if (isConnectedUser) {
            return true;
        }
        return false;
    }

    getCurrentToken(): Token {
        return JSON.parse(this.getStorage().getItem(AppConstant.MAUREVAIR_TOKEN));
    }

    getStorage(): Storage {
        return localStorage;
    }

    contentTypeHeader(): HttpHeaders {
        const token = this.getCurrentToken();
        let headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.append('Authorization', 'Bearer ' + token.access_token);
        return headers;
    }

    contentTypeHeaderForMultipartFile(): HttpHeaders {
        const token = this.getCurrentToken();
        return new HttpHeaders().set('Authorization', 'Bearer ' + token.access_token);
    }


}