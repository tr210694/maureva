import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Flight } from '../domain/flight';
import { AuthenticationService } from './authentication-service';

@Injectable({ providedIn: 'root' })
export class FlightService {
    private API_MAUREAIR: string = environment.API_ENDPOINT;

    constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

    loadAllFlights(): Observable<any[]> {
        const url: string = this.API_MAUREAIR + 'all-flights';
        return this.http
            .get<any[]>(url, { headers: this.authenticationService.contentTypeHeader() })
            .pipe(catchError(this.handleError));
    }

    uploadFlightDetails(file: File): Observable<any> {
        const url: string = this.API_MAUREAIR + 'flights';
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        console.log(formdata);
        return this.http.post(url, formdata, { headers: this.authenticationService.contentTypeHeaderForMultipartFile() });
    }

    createReservation(flight: Flight): Observable<Flight> {
        const url: string = this.API_MAUREAIR + '/flight-bookings';
        return this.http
            .post<any>(url, JSON.stringify(flight), { headers: this.authenticationService.contentTypeHeader() })
            .pipe(catchError(this.handleError));

    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred:', error);
        return throwError(error);
    }
}
