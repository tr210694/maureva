import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from './authentication-service';

@Injectable({ providedIn: 'root' })
export class AirportService {
    private API_MAUREAIR: string = environment.API_ENDPOINT;

    constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

    loadAllFlights(): Observable<any[]> {
        const url: string = this.API_MAUREAIR + 'airports';
        return this.http
            .get<any[]>(url, { headers: this.authenticationService.contentTypeHeader() })
            .pipe(catchError(this.handleError));
    }

    uploadAirportDetails(file: File): Observable<any> {
        const url: string = this.API_MAUREAIR + 'airports';
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        console.log(formdata);
        return this.http.post(url, formdata, { headers: this.authenticationService.contentTypeHeaderForMultipartFile() });
    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred:', error);
        return throwError(error);
    }
}
