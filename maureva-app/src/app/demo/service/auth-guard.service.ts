import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad } from '@angular/router';
import { AppConstant } from 'src/app/app-constant';
import { AuthenticationService } from './authentication-service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanLoad {
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router) {
    }

    canActivate() {
        if (this.authenticationService.isLogged()) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }

    canLoad(): boolean {
        return this.isLogged();
    }

    private isLogged(): boolean {
        if (this.authenticationService.isLogged()) {
            return true;
        }
        this.authenticationService.getStorage().removeItem(AppConstant.MAUREVAIR_TOKEN);
        this.router.navigate(['/login']);
        return false;
    }
}
