import { BookingInfo } from "./booking";

export class Flight {
    uid: number;
    id: string;
    carrier: string;
    flightNumber: string;
    origin: string;
    destination: string;
    departureTime: Date;
    arrivalTime: Date;
    bookingInfos: BookingInfo[]= [];
}