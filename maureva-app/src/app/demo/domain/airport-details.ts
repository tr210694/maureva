import { BookingInfo } from "./booking";

export class AirportDetail {
    uid: number;
    carrier: string;
    flightNumber: number;
    origin: string;
    destination: string;
    departureTime: Date;
    arrivalTime: Date;
    bookingInfo: BookingInfo[] = [];
    seatAvailable: number;

}