import { BookingInfo } from "./booking";

export class FlightBooking {
    id: number;
    bookingReference: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    passportNumber: string;
    cabinClass: string;
}