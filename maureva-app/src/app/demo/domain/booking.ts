export class BookingInfo {
    uid: number;
    cabinClass: string;
    bookingCount: number;
}