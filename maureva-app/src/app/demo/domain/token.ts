export class Token {
    currentUser: any;
    access_token: string;
    token_type: string;
    scope: string;
    strTokenGenerationDate: string;

}