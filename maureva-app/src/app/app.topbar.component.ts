import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstant } from './app-constant';
import { AppMainComponent } from './app.main.component';
import { AuthenticationService } from './demo/service/authentication-service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {

  userLogin: string;

  constructor(public app: AppMainComponent, private authenticationService: AuthenticationService, 
    private router: Router) { }

  ngOnInit() {
    this.userLogin = this.getConnectedUser();
  }

  private getConnectedUser(): string {
    if (this.authenticationService.getConnectedUser() && this.authenticationService.getConnectedUser().username) {
      console.log(this.authenticationService.getConnectedUser().username);
      return this.authenticationService.getConnectedUser().username;
    }
    return '';
  }

  logout(): void {
    this.authenticationService.getStorage().removeItem(AppConstant.MAUREVAIR_TOKEN);
    this.router.navigate(['login']);
  }
}
